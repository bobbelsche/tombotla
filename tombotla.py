#!/usr/bin/python3
# coding=utf-8
# tombotla.py
# a simple tombola Discord bot
import os
from dotenv import load_dotenv
import discord
from discord.ext import commands

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

def get_prefix(bot, message):
	prefixes = ['tomb!','tb!','tombotla!','tombola!']
	if not message.guild:
		return prefixes
	return commands.when_mentioned_or(*prefixes)(bot, message)

bot = commands.Bot(command_prefix=get_prefix,description='tombotla: A simple tombola management bot',case_insensitive=True)

cogs = ['cogs.Management']

@bot.event
async def on_ready():
	await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.listening, name='@tombotla help'))
	print(f'{bot.user.name} successfully connected to Discord, ready for managing tombolas!')
	for cog in cogs:
		bot.load_extension(cog)
	return

bot.run(TOKEN, bot=True, reconnect=True)